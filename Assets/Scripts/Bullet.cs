﻿using System;
using UnityEngine;


public class Bullet : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private int damage = 1;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] public AttackController attackcontroller;

    public GameObject Host { get; set; }

    public SpriteRenderer Sprite => sprite;

    public void Fire(Vector2 _direction, float _speed = 10f)
    {
        rb.velocity = _direction * _speed;
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject.name.Contains(Host.name)) return;

        Debug.Log(_col.gameObject.name);

        if (_col.gameObject.TryGetComponent<Player>(out var _player))
        {   
            _player.DecreaseHp(damage);
        }

        if (_col.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            damage = attackcontroller.currentWeapon.Damage;
            _damageable.ApplyDamage(Host ,damage);
        }
        Destroy(gameObject);
    }
    
    private static Bullet bulletPrefab;
    private const string path = "Bullet";
    public static Bullet GetBullet(GameObject _host)
    {
        if (!bulletPrefab)
            bulletPrefab = Resources.Load<Bullet>(path);

        var _bullet = Instantiate(bulletPrefab);
        _bullet.Host = _host;
        
        return _bullet;
    }
}
