using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private int damage = 1;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private AttackController attackcontroller;

    public GameObject Host { get; set; }
    public Player player;

    public SpriteRenderer Sprite => sprite;

    private void OnTriggerEnter2D(Collider2D _col)
    {
        damage = attackcontroller.currentWeapon.Damage;

        if (_col.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            if (_col.gameObject.TryGetComponent<Player>(out var _player))
            {
                return;
            }
            _damageable.ApplyDamage(Host, damage);
        }
    }

}
