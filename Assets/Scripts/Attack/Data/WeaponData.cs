﻿using UnityEngine;

public enum AttackType
{
    MeleeAttack,
    RangeAttack
}
[CreateAssetMenu(menuName = "WeaponData")]
public class WeaponData : ScriptableObject
{
    [field: SerializeField] public string Name { get; private set; }
    [field: SerializeField] public int Damage { get; private set; }
    [field: SerializeField] public Color weaponColor { get; private set; } = Color.white;
    [field: SerializeField] public AttackType AttackType { get; private set; }
}
