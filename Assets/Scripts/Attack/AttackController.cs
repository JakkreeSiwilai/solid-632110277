using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AttackController : MonoBehaviour
{
    public static event Action<WeaponData> OnChangedWeapon; //Attack

    [Header("Attack")]
    [SerializeField] public WeaponData currentWeapon;      //Attack
    [SerializeField] private SpriteRenderer weaponSprite;   //Attack
    [SerializeField] private Transform firePoint;           //Attack && Movement
    [SerializeField] private GameObject meleeHitBox;        //Attack
    private bool isAttacking;                               //Attack

    public void ChangeWeapon(WeaponData _newWeapon)
    {
        currentWeapon = _newWeapon;
        OnChangedWeapon?.Invoke(currentWeapon);
    }

    public void PerformAttack(InputAction.CallbackContext _context)
    {
        if (!_context.performed || !currentWeapon) return;

        switch (currentWeapon.AttackType)
        {
            case AttackType.MeleeAttack:
                MeleeAttack();
                break;
            case AttackType.RangeAttack:
                RangeAttack();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }


    private void MeleeAttack()
    {
        if (isAttacking) return;

        weaponSprite.color = currentWeapon.weaponColor;
        StartCoroutine(IEAttack());
    }

    private void RangeAttack()
    {
        var _bullet = Bullet.GetBullet(gameObject);
        _bullet.transform.position = firePoint.position;
        _bullet.Sprite.color = currentWeapon.weaponColor;
        _bullet.Fire(firePoint.right);
        _bullet.attackcontroller = this;
    }

    private IEnumerator IEAttack()
    {
        isAttacking = true;
        meleeHitBox.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        meleeHitBox.SetActive(false);
        isAttacking = false;
    }

}
