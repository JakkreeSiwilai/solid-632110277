using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamageable
{
    //public static event Action<WeaponData> OnChangedWeapon;     //Attack
    public UnityEvent<int> onHpChanged;

    private const int MaxHp = 5;

    [Header("Movement")]
    [SerializeField] private Rigidbody2D rb;                    //Movement && HP
    //[SerializeField] private Transform body;                    //Movement
    //[SerializeField] private float moveSpeed = 5f;              //Movement
    //[SerializeField] private float jumpForce = 7f;              //Movement
    //[SerializeField] private LayerMask groundLayer;             //Movement

    [Header("Health")] 
    [SerializeField] private int currentHp = MaxHp;
    
    [Header("Attack")]
    //[SerializeField] private WeaponData currentWeapon;        //Attack
    //[SerializeField] private SpriteRenderer weaponSprite;     //Attack
    //[SerializeField] private Transform firePoint;             //Attack && Movement
    //[SerializeField] private GameObject meleeHitBox;          //Attack
    //private bool isAttacking;                                 //Attack
    
    [SerializeField] private Transform spawnPoint;              //HP
    //private Vector2 moveInput;                                //Movement
    //private bool isGrounded;                                  //Movement
    
    #region Attack
    /*
    public void ChangeWeapon(WeaponData _newWeapon)
    {
        currentWeapon = _newWeapon;
        OnChangedWeapon?.Invoke(currentWeapon);
    }
    
    public void PerformAttack(InputAction.CallbackContext _context)
    {
        if (!_context.performed || !currentWeapon) return;

        switch (currentWeapon.AttackType)
        {
            case AttackType.MeleeAttack:
                MeleeAttack();
                break;
            case AttackType.RangeAttack:
                RangeAttack();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    

    private void MeleeAttack()
    {
        if (isAttacking) return;
        
        weaponSprite.color = currentWeapon.weaponColor;
        StartCoroutine(IEAttack());
    }

    private void RangeAttack()
    {
        var _bullet = Bullet.GetBullet(gameObject);
        _bullet.transform.position = firePoint.position;
        _bullet.Sprite.color = currentWeapon.weaponColor;
        _bullet.Fire(firePoint.right);
    }

    private IEnumerator IEAttack()
    {
        isAttacking = true;
        meleeHitBox.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        meleeHitBox.SetActive(false);
        isAttacking = false;
    }
    */
    #endregion

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    
    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death();
        }
        onHpChanged?.Invoke(currentHp);
    }

    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }

    private void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }

    public void ApplyDamage(GameObject _source, int _damage)
    {
        DecreaseHp(_damage);
    }

    #endregion

    #region Movement
    /*
    public void OnMove(InputAction.CallbackContext _context)
    {
        moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!isGrounded) return;
        
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
    }
    
    private readonly float checkGroundRayLenght = 0.6f;
    
    private void FixedUpdate()
    {
        //UpdateMovement
        rb.velocity = new Vector2(moveInput.x * moveSpeed, rb.velocity.y);
        
        // Flip the player sprite when changing direction
        if (moveInput.x != 0)
        {
            body.localScale = new Vector3(Mathf.Sign(moveInput.x), 1f, 1f);
            float _rotate = Mathf.Sign(moveInput.x) > 0 ? 0 : 180f;
            firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
        }
        
        //CheckGround
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, groundLayer);
        
        isGrounded = _hit.collider != null;
    }
    
    private void UpdateMovement()
    {
        
    }

    private void CheckGround()
    {
        
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }
    */
    #endregion
}
