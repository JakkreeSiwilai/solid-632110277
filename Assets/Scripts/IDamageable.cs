using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    public void ApplyDamage(GameObject _source, int _damage);
    private void Death(GameObject _source) { }
}
